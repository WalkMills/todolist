from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateForm


@login_required
def project_list(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"project_list": projects}
    return render(request, "projects/list.html", context)


@login_required
def project_detail(request, id):
    details = Project.objects.get(id=id)
    context = {"project_detail": details}
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")

    else:
        form = CreateForm()
    context = {"create_form": form}
    return render(request, "projects/create.html", context)
