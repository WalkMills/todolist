from django.urls import path
from accounts.views import login_view, logout_view, sign_up

urlpatterns = [
    path("login/", login_view, name="login"),
    path("logout/", logout_view, name="logout"),
    path("signup/", sign_up, name="signup"),
]
